package alm.works.lockers.inherited;

import org.testng.annotations.Test;

/**
 * Тест утилитного класса {@link ReentrantEntityLocker}.
 *
 * @author sbt-yablokov-mv
 * @ created on 20.02.2018
 */
public class ReentrantEntityLockerTest {
    @Test
    public void lock() throws Exception {
    }

    @Test
    public void tryLock() throws Exception {
    }

    @Test
    public void unlock() throws Exception {
    }

    @Test
    public void executeWithLock_RunnableCase() throws Exception {
    }

    @Test
    public void executeWithLock_CallableCase() throws Exception {
    }

    @Test
    public void tryExecuteWithLock() throws Exception {
    }

    private Entity buildEntity(String id) {
        return buildEntity(id, "some value");
    }

    private Entity buildEntity(String id, String someField) {
        return new TestEntity().withId(id).withSomeField(someField);
    }
}