package alm.works.lockers.inherited;

public class TestEntity implements Entity<String> {
    private String id;
    private String someField;

    @Override
    public String getId() {
        return id;
    }

    void setId(String id) {
        this.id = id;
    }

    String getSomeField() {
        return someField;
    }

    void setSomeField(String someField) {
        this.someField = someField;
    }

    TestEntity withId(String id) {
        setId(id);
        return this;
    }

    TestEntity withSomeField(String someField) {
        setSomeField(someField);
        return this;
    }
}
