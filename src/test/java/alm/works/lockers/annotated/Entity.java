package alm.works.lockers.annotated;

/**
 * Сущность, используемая для тестирования механизма работы
 * {@link ReentrantEntityLocker}.
 *
 * @author sbt-yablokov-mv
 * @ created on 14.02.2018
 */
class Entity {
    @ID
    private String id;
    private String someField;

    String getId() {
        return id;
    }

    void setId(String id) {
        this.id = id;
    }

    String getSomeField() {
        return someField;
    }

    void setSomeField(String someField) {
        this.someField = someField;
    }

    Entity withId(String id) {
        setId(id);
        return this;
    }

    Entity withSomeField(String someField) {
        setSomeField(someField);
        return this;
    }
}
