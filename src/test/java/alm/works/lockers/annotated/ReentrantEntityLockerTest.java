package alm.works.lockers.annotated;

import alm.works.lockers.EntityLocker;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertEquals;

/**
 * Тест утилитного класса {@link ReentrantEntityLocker}.
 *
 * @author sbt-yablokov-mv
 * @ created on 18.02.2018
 */
public class ReentrantEntityLockerTest {
    @Test(expectedExceptions = IllegalArgumentException.class)
    public void constructor_NullEntity_ExceptionExpected() throws Exception {
        new ReentrantEntityLocker<>(null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void constructor_EntityWithoutId_ExceptionExpected() throws Exception {
        class IllegalEntity {
            private final Integer field1 = 1;
            private final Integer field2 = 2;
        }

        IllegalEntity entity = new IllegalEntity();
        new ReentrantEntityLocker<>(entity);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void constructor_EntityWithNullId_ExceptionExpected() throws Exception {
        new ReentrantEntityLocker<>(buildEntity(null));
    }

    @Test(threadPoolSize = 30, invocationCount = 100, invocationTimeOut = 10000)
    public void lockAndUnlock_EntityLock_ReentrantCase() throws Exception {
        Entity entity = buildEntity("lockAndUnlock_EntityLock_ReentrantCase");

        EntityLocker entityLocker = new ReentrantEntityLocker<>(entity);
        entityLocker.lock();
        try {
            entityLocker.lock();
            imitateWorkAndUnlock(entityLocker);
        } finally {
            entityLocker.unlock();
        }
    }

    @Test(threadPoolSize = 30, invocationCount = 100, invocationTimeOut = 10000)
    public void tryLock_EntityLock_ReentrantCase() throws Exception {
        Entity entity = buildEntity("tryLock_EntityLock_ReentrantCase");
        EntityLocker entityLocker = new ReentrantEntityLocker<>(entity);

        if (entityLocker.tryLock(1, TimeUnit.MICROSECONDS)) {
            imitateWorkAndUnlock(entityLocker);
        }

        if (entityLocker.tryLock(1, TimeUnit.MILLISECONDS)) {
            imitateWorkAndUnlock(entityLocker);
        }

        if (entityLocker.tryLock(1, TimeUnit.SECONDS)) {
            imitateWorkAndUnlock(entityLocker);
        }
    }

    @Test(threadPoolSize = 30, invocationCount = 100, invocationTimeOut = 10000)
    public void tryLock_GlobalLock_ReentrantCase() throws Exception {
        EntityLocker entityLocker = new ReentrantEntityLocker<>();

        if (entityLocker.tryLock(1, TimeUnit.MICROSECONDS)) {
            executeAndUnlock(entityLocker, () -> {
                try {
                    if (entityLocker.tryLock(1, TimeUnit.MILLISECONDS)) {
                        imitateWorkAndUnlock(entityLocker);
                    }
                } catch (InterruptedException ignore) {
                }
            });
        }
    }

    @Test(threadPoolSize = 30, invocationCount = 100, invocationTimeOut = 10000)
    public void lockAndUnlock_GlobalAndEntityLockConsistently_ReentrantCase() throws Exception {
        EntityLocker globalLocker = new ReentrantEntityLocker<>();
        globalLocker.lock();
        executeAndUnlock(globalLocker, () -> {
            Entity entity = buildEntity("lockAndUnlock_GlobalAndEntityLockConsistently_ReentrantCase");

            ReentrantEntityLocker<Object> entityLocker = new ReentrantEntityLocker<>(entity);
            entityLocker.lock();
            executeAndUnlock(entityLocker, () -> {
                entityLocker.lock();
                imitateWorkAndUnlock(entityLocker);
            });
        });
    }

    @Test(threadPoolSize = 30, invocationCount = 100, invocationTimeOut = 10000)
    public void lockAndUnlock_EntityAndGlobalLockConsistently_ReentrantCase() throws Exception {
        Entity entity = buildEntity("lockAndUnlock_EntityAndGlobalLockConsistently_ReentrantCase");

        EntityLocker entityLocker = new ReentrantEntityLocker<>(entity);
        entityLocker.lock();
        executeAndUnlock(entityLocker, () -> {
            ReentrantEntityLocker<Object> globalLocker = new ReentrantEntityLocker<>();
            globalLocker.lock();
            executeAndUnlock(globalLocker, () -> {
                entityLocker.lock();
                imitateWorkAndUnlock(entityLocker);
            });
        });
    }

    @Test(expectedExceptions = IllegalMonitorStateException.class)
    public void unlock_NotLockedEntity_ExceptionExcepted() throws Exception {
        Entity entity = buildEntity("unlock_NotLockedEntity_ExceptionExcepted");

        EntityLocker entityLocker = new ReentrantEntityLocker<>(entity);
        entityLocker.unlock();
    }

    @Test(threadPoolSize = 30, invocationCount = 100, invocationTimeOut = 10000)
    public void executeWithLock_RunnableCase() throws Exception {
        Entity entity = buildEntity("executeWithLock_RunnableCase", "initial value");

        EntityLocker entityLocker = new ReentrantEntityLocker<>(entity);
        entityLocker.executeWithLock(() -> {
            imitateWork();
            entity.setSomeField("changed value");
        });
        assertEquals(entity.getSomeField(), "changed value");
    }

    @Test(threadPoolSize = 30, invocationCount = 100, invocationTimeOut = 10000)
    public void executeWithLock_CallableCase() throws Exception {
        Entity entity = buildEntity("executeWithLock_CallableCase", "initial value");

        EntityLocker entityLocker = new ReentrantEntityLocker<>(entity);
        entityLocker.executeWithLock(() -> {
            imitateWork();
            entity.setSomeField("changed value");
            return null;
        });
        assertEquals(entity.getSomeField(), "changed value");
    }

    @Test(threadPoolSize = 30, invocationCount = 100, invocationTimeOut = 10000)
    public void tryExecuteWithLock() throws Exception {
        Entity entity = buildEntity("tryExecuteWithLock", "initial value");

        EntityLocker entityLocker = new ReentrantEntityLocker<>(entity);
        entityLocker.tryExecuteWithLock(100, TimeUnit.MILLISECONDS, () -> {
            entity.setSomeField("changed value");
        });
        assertEquals(entity.getSomeField(), "changed value");
    }

    private void executeAndUnlock(EntityLocker entityLocker, Runnable runnable) {
        try {
            runnable.run();
        } finally {
            entityLocker.unlock();
        }
    }

    private void imitateWorkAndUnlock(EntityLocker entityLocker) {
        try {
            imitateWork(25);
        } finally {
            entityLocker.unlock();
        }
    }

    private void imitateWork() {
        imitateWork(25);
    }

    private void imitateWork(long timeout) {
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException ignore) {
        }
    }

    private Entity buildEntity(String id) {
        return buildEntity(id, "some value");
    }

    private Entity buildEntity(String id, String someField) {
        return new Entity().withId(id).withSomeField(someField);
    }
}