package alm.works.lockers;

/**
 * Исключение, указывающее на факт обнаружения dead lock'а.
 *
 * @author sbt-yablokov-mv
 * @ created on 20.02.2018
 */
public class DeadLockException extends RuntimeException {
    DeadLockException(String message) {
        super(message);
    }
}
