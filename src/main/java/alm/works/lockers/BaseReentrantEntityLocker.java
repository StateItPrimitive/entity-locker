package alm.works.lockers;

import alm.works.lockers.DeadLockTraceUtility.TraceNode;
import javafx.util.Pair;
import org.slf4j.Logger;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Базовый класс для реализаций {@link EntityLocker}, работающих на основе {@link java.util.concurrent.locks.ReentrantLock}.
 *
 * <p>Данный класс реализует основную часть логики предоставления "защищенного кода"
 * на основе ключей DO-сущностей, предоставляемых в класс посредством конструктора.</p>
 *
 * <p>Реализация основана на идиоме {@link java.util.concurrent.locks.ReentrantLock}. Таким образом,
 * в пределах одного потока имеется возможность сколько-угодно раз рекурсивно зайти
 * вглубь в "защищенный код" в пределах одной и той же пары {@code <Класс DO; значение ключа DO>}.</p>
 *
 * @author sbt-yablokov-mv
 * @ created on 18.02.2018
 */
public abstract class BaseReentrantEntityLocker<T> implements EntityLocker {
    @SuppressWarnings("squid:S2176")
    private static class ReentrantLock extends java.util.concurrent.locks.ReentrantLock {
        @Override
        public Thread getOwner() {
            return super.getOwner();
        }
    }

    /**
     * Коллекция, используемая для хранения информации о сущностях, в разрезе которых исполняется "защищенный код".
     *
     * <p>В качестве ключа выступает пара {класс сущности:значение ключа}, олицетворяя пары {таблица:значение PK},
     * а в качестве знаения - блокировщик.</p>
     */
    private static final Map<Pair<Class<?>, Object>, ReentrantLock> LOCKED_KEYS = new ConcurrentHashMap<>();

    /** Коллекция заблокированных текущим потоком ключей */
    private static final ThreadLocal<Set<ReentrantLock>> THREAD_LOCKED_KEYS = ThreadLocal.withInitial(HashSet::new);

    /**
     * Коллекция, используемая для хранения данных о связанных с entity блокировщиках, которыми хочет завладеть поток.
     *
     * <p>В качестве используется сама сущность {@link Thread}, а не {@link Thread#getId()}, в виду того факта,
     * что идентификаторы завершивших свое выполнение потоков могут быть переиспользованы позднее созданными потоками.</p>
     */
    private static final Map<Thread, Pair<Class<?>, Object>> LOCK_WISHFUL_KEYS_BY_THREADS = new ConcurrentHashMap<>();

    /**
     * Пороговое значение количества удерживаемых блокировок различных entity в пределах отдельно взятого потока
     * по достижению которого происходит эскалация блокировок этого потока до глобальной
     */
    private static final Integer LOCK_ESCALATION_THRESHOLD = 15;

    /**
     * Пороговое значение количества удерживаемых блокировок различных entity в пределах
     * отдельно взятого потока по достижению которого происходит деэскалация глобальной блокировки
     * до уровня блокировок на отдельно взятых entities.
     *
     * <p>Описанный механизм деэскалация актуален лишь в случае, когда эскалация уже была произведена,
     * т.е. данный параметр работает в паре с {@link #LOCK_ESCALATION_THRESHOLD}</p>
     */
    private static final Integer LOCK_DEESCALATION_THRESHOLD = 10;

    /** Произошла ли эскалация блокировок блокировок с уровня entity до global. */
    private static final ThreadLocal<Boolean> THREAD_KEY_LOCK_ESCALATED = ThreadLocal.withInitial(() -> false);

    private static final ReentrantReadWriteLock GLOBAL_LOCKER = new ReentrantReadWriteLock();

    private final Pair<Class<?>, Object> lockKey;
    private final boolean globalLock;

    /**
     * Данный конструктор предназначен для глобальных блокировок.
     *
     * <p>"Защищенный код", исполняемый в пределах глобальной блокировки,
     * не подвержен конкуренции других потоков, т.е. такой код исполняется в абсолютно эксклюзивном режиме.
     * Таким образом, ни один другой поток не может выполняться в границах "защищенного кода" до тех пор,
     * пока поток, захвативший глобальную блокировку не отпустит её.</p>
     */
    protected BaseReentrantEntityLocker() {
        this(null, true);
    }

    /**
     * Данный конструктор предназначен для блокировок ключа DO-сущности.
     *
     * <p>"Защищенный код", исполняемый в пределах блокировки значения ключа DO-сущности,
     * является недоступным другим потокам в пределах того же значения ключа DO-сущности.
     * Таким образом, ни один другой поток не может выполниться в границах "защищенного кода"
     * в разрезе блокировки значения ключа DO-сущности до тех пор, пока уже выполняющийся
     * в этих границах в разрезе того же значения ключа DO-сущности поток не выйдет за их пределы.</p>
     */
    protected BaseReentrantEntityLocker(T entity) {
        this(entity, false);
    }

    private BaseReentrantEntityLocker(T entity, boolean globalLock) {
        this.lockKey = globalLock ? null : getKey(entity);
        this.globalLock = globalLock;
    }

    private Pair<Class<?>, Object> getKey(T entity) {
        if (entity != null) {
            return new Pair<>(entity.getClass(), getEntityId(entity));
        }
        throw new IllegalArgumentException("Недопустимо использование null Entity в качестве экземпляра блокировки.");
    }

    /**
     * Получить значение идентификатора сущности
     *
     * @param entity сущность, значение идентификатора которой необходимо получить
     * @return значение идентификатора (ключевого поля) сущности
     */
    protected abstract Object getEntityId(T entity);

    @Override
    public void lock() {
        if (globalLock || isNeedThreadLockEscalation()) {
            lockGlobal();
        }
        if (!globalLock) {
            lockKey();
        }
    }

    private Boolean isNeedThreadLockEscalation() {
        Boolean threadLockEscalated = THREAD_KEY_LOCK_ESCALATED.get();
        refreshThreadLockEscalationCondition();
        return !threadLockEscalated && THREAD_KEY_LOCK_ESCALATED.get();
    }

    private void refreshThreadLockEscalationCondition() {
        THREAD_KEY_LOCK_ESCALATED.set(!globalLock &&
                (THREAD_KEY_LOCK_ESCALATED.get() || (GLOBAL_LOCKER.getReadHoldCount() + 1 >= LOCK_ESCALATION_THRESHOLD)));
    }

    private void lockGlobal() {
        boolean needReLockThreadReadLocks = (GLOBAL_LOCKER.getReadHoldCount() > 0) && !GLOBAL_LOCKER.isWriteLockedByCurrentThread();
        Map<ReentrantLock, Integer> holdLocksCountByThreadLockers = new HashMap<>();

        if (needReLockThreadReadLocks) {
            for (ReentrantLock keyLocker : THREAD_LOCKED_KEYS.get()) {
                int holdCount = keyLocker.getHoldCount();
                holdLocksCountByThreadLockers.put(keyLocker, holdCount);
                for (int i = 0; i < holdCount; ++i) {
                    keyLocker.unlock();
                    GLOBAL_LOCKER.readLock().unlock();
                }
            }
        }

        GLOBAL_LOCKER.writeLock().lock();

        if (needReLockThreadReadLocks) {
            for (Map.Entry<ReentrantLock, Integer> entry : holdLocksCountByThreadLockers.entrySet()) {
                for (int i = 0; i < entry.getValue(); ++i) {
                    GLOBAL_LOCKER.readLock().lock();
                    entry.getKey().lock();
                }
            }
        }
    }

    private void lockKey() {
        GLOBAL_LOCKER.readLock().lock();

        ReentrantLock keyLocker = putKeyIfAbsentAndGet();

        Pair<Boolean, String> checkResult = checkForDeadLock(keyLocker);
        if (checkResult.getKey()) {
            unlock(false);
            throw new DeadLockException(checkResult.getValue());
        }

        lockKey(keyLocker);
    }

    private Pair<Boolean, String> checkForDeadLock(ReentrantLock keyLocker) {
        Deque<TraceNode> lockStack = new LinkedList<>();
        if (keyLocker.isLocked() && !keyLocker.isHeldByCurrentThread() &&
                isThereDeadLock(Thread.currentThread(), keyLocker.getOwner(), lockStack)) {
            return new Pair<>(true, DeadLockTraceUtility.buildTraceMessage(lockStack));
        }
        return new Pair<>(false, null);
    }

    private boolean isThereDeadLock(Thread lockWisherThread, Thread lockOwnerThread, Deque<TraceNode> lockStack) {
        if (lockOwnerThread != null) {
            lockStack.push(new TraceNode(lockKey, lockWisherThread, lockOwnerThread));

            Pair<Class<?>, Object> wishedToLockingEntity = LOCK_WISHFUL_KEYS_BY_THREADS.get(lockOwnerThread);
            if (wishedToLockingEntity != null) {
                ReentrantLock keyLocker = LOCKED_KEYS.get(wishedToLockingEntity);
                return (keyLocker != null) && keyLocker.isLocked() &&
                        (keyLocker.isHeldByCurrentThread() || isThereDeadLock(lockOwnerThread, keyLocker.getOwner(), lockStack));
            }
        }
        return false;
    }

    @SuppressWarnings("squid:S2222")
    private void lockKey(ReentrantLock keyLocker) {
        LOCK_WISHFUL_KEYS_BY_THREADS.put(Thread.currentThread(), lockKey);
        try {
            keyLocker.lock();
            THREAD_LOCKED_KEYS.get().add(keyLocker);
        } finally {
            LOCK_WISHFUL_KEYS_BY_THREADS.remove(Thread.currentThread());
        }
    }

    @Override
    public boolean tryLock(long timeout, TimeUnit timeUnit) throws InterruptedException {
        if (globalLock) {
            return GLOBAL_LOCKER.writeLock().tryLock(timeout, timeUnit);
        } else {
            return tryLockKey(timeout, timeUnit);
        }
    }

    private boolean tryLockKey(long timeout, TimeUnit timeUnit) throws InterruptedException {
        long readLockStartTime = System.nanoTime();
        if (GLOBAL_LOCKER.readLock().tryLock(timeout, timeUnit)) {
            try {
                ReentrantLock keyLocker = putKeyIfAbsentAndGet();

                Pair<Boolean, String> checkResult = checkForDeadLock(keyLocker);
                if (checkResult.getKey()) {
                    unlock(false);
                    getLogger().warn(checkResult.getValue());
                    return false;
                }

                long estimatedTime = System.nanoTime() - readLockStartTime;
                long remainingTimeout = timeUnit.toNanos(timeout) - estimatedTime;
                if (tryLockKey(keyLocker, remainingTimeout, TimeUnit.NANOSECONDS)) {
                    THREAD_LOCKED_KEYS.get().add(keyLocker);
                    return true;
                } else {
                    GLOBAL_LOCKER.readLock().unlock();
                }
            } catch (InterruptedException e) {
                GLOBAL_LOCKER.readLock().unlock();
                throw e;
            }
        }
        return false;
    }

    private ReentrantLock putKeyIfAbsentAndGet() {
        return LOCKED_KEYS.computeIfAbsent(lockKey, key -> new ReentrantLock());
    }

    protected abstract Logger getLogger();

    @SuppressWarnings("squid:S2222")
    private boolean tryLockKey(ReentrantLock keyLocker, long timeout, TimeUnit timeUnit) throws InterruptedException {
        if (timeout <= 0) {
            return false;
        }

        LOCK_WISHFUL_KEYS_BY_THREADS.put(Thread.currentThread(), lockKey);
        try {
            return keyLocker.tryLock(timeout, timeUnit);
        } finally {
            LOCK_WISHFUL_KEYS_BY_THREADS.remove(Thread.currentThread());
        }
    }

    @Override
    public void unlock() {
        unlock(true);
    }

    private void unlock(boolean needUnlockKey) {
        if (!globalLock) {
            if (needUnlockKey) unlockKey();
            GLOBAL_LOCKER.readLock().unlock();
        }

        if (globalLock || isNeedThreadLockDeescalation()) {
            GLOBAL_LOCKER.writeLock().unlock();
        }
    }

    private Boolean isNeedThreadLockDeescalation() {
        Boolean threadLockEscalated = THREAD_KEY_LOCK_ESCALATED.get();
        refreshThreadLockDeescalationCondition();
        return threadLockEscalated && !THREAD_KEY_LOCK_ESCALATED.get();
    }

    private void refreshThreadLockDeescalationCondition() {
        THREAD_KEY_LOCK_ESCALATED.set(!globalLock &&
                (THREAD_KEY_LOCK_ESCALATED.get() || (GLOBAL_LOCKER.getReadHoldCount() > LOCK_DEESCALATION_THRESHOLD)));
    }

    private void unlockKey() {
        ReentrantLock reentrantLock = LOCKED_KEYS.get(lockKey);
        if (reentrantLock != null) {
            reentrantLock.unlock();
            if (reentrantLock.getHoldCount() == 0) {
                THREAD_LOCKED_KEYS.get().remove(reentrantLock);
            }
            removeKeyAsNeeded(lockKey, reentrantLock);
        } else {
            throw new IllegalMonitorStateException();
        }
    }

    private void removeKeyAsNeeded(Pair<Class<?>, Object> entityInfo, ReentrantLock reentrantLock) {
        if (!reentrantLock.isLocked() && GLOBAL_LOCKER.writeLock().tryLock()) {
            try {
                LOCKED_KEYS.remove(entityInfo);
            } finally {
                GLOBAL_LOCKER.writeLock().unlock();
            }
        }
    }

    @Override
    public void executeWithLock(Runnable runnable) {
        lock();
        try {
            runnable.run();
        } finally {
            unlock();
        }
    }

    @Override
    public <V> V executeWithLock(Callable<V> callable) throws Exception {
        lock();
        try {
            return callable.call();
        } finally {
            unlock();
        }
    }

    @Override
    public boolean tryExecuteWithLock(long timeout, TimeUnit timeUnit, Runnable runnable) throws InterruptedException {
        if (tryLock(timeout, timeUnit)) {
            try {
                runnable.run();
                return true;
            } finally {
                unlock();
            }
        }
        return false;
    }
}
