package alm.works.lockers;

import javafx.util.Pair;

import java.util.Deque;

/**
 * Вспомогательный класс, предоставляющий удобные сущности для трассировки dead lock,
 * а также формирования сообщений об ошибках на основе предоставляемых сущностей.
 *
 * @author sbt-yablokov-mv
 * @ created on 21.02.2018
 */
class DeadLockTraceUtility {
    static class TraceNode {
        private final Pair<Class<?>, Object> lockedKey;
        private final Thread lockWisherThread;
        private final Thread lockOwnerThread;

        TraceNode(Pair<Class<?>, Object> lockedKey, Thread lockWisherThread, Thread lockOwnerThread) {
            this.lockedKey = lockedKey;
            this.lockWisherThread = lockWisherThread;
            this.lockOwnerThread = lockOwnerThread;
        }

        Pair<Class<?>, Object> getLockedKey() {
            return lockedKey;
        }

        Thread getLockWisherThread() {
            return lockWisherThread;
        }

        Thread getLockOwnerThread() {
            return lockOwnerThread;
        }
    }

    private DeadLockTraceUtility() {
        throw new UnsupportedOperationException();
    }

    /**
     * Данный метод на основе параметра {@code lockStack} построит сообщение, содержащее информацию о
     * стеке зависимостей возникшего dead lock в человекочитаемом формате.
     */
    static String buildTraceMessage(Deque<TraceNode> lockStack) {
        TraceNode head = lockStack.getFirst();

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(buildTraceMessageForHead(head));

        for (TraceNode node : lockStack) {
            stringBuilder.append(buildTraceMessageForNode(node));
        }

        return stringBuilder.toString();
    }

    private static String buildTraceMessageForHead(TraceNode head) {
        return String.format("При попытке блокировки потоком {id: %s, name: %s} сущности {class: %s, key: %s}" +
                        "был обнаружен потенциальный dead lock:\n",
                head.getLockWisherThread().getId(), head.getLockWisherThread().getName(),
                head.getLockedKey().getKey(), head.getLockedKey().getValue());
    }

    private static String buildTraceMessageForNode(TraceNode node) {
        return String.format("\n\tЖелающий заблокировать сущность поток {id: %s, name: %s}." +
                        " Блокируемая сущность {class: %s, key: %s}." +
                        " Владеющий блокировкой сущности поток {id: %s, name: %s}.",
                node.getLockWisherThread().getId(), node.getLockWisherThread().getName(),
                node.getLockedKey().getKey(), node.getLockedKey().getValue(),
                node.getLockOwnerThread().getId(), node.getLockOwnerThread().getName());
    }
}
