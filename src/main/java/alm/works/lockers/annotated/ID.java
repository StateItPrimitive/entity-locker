package alm.works.lockers.annotated;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Аннотация, используемая в качестве маркера для поля, являющегося ключом DO-сущности,
 * на основе которой происходит блокировка доступа к "защищенному коду" в утилитном классе
 * {@link ReentrantEntityLocker}.
 *
 * @author sbt-yablokov-mv
 * @ created on 18.02.2018
 * @see ReentrantEntityLocker
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ID {
}
