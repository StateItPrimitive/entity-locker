package alm.works.lockers.annotated;

import alm.works.lockers.BaseReentrantEntityLocker;
import alm.works.lockers.EntityLocker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Реализация {@link EntityLocker}, работающая с DO-сущностями,
 * одно из полей которых обязано иметь аннотацию-маркер {@link ID}.
 *
 * @author sbt-yablokov-mv
 * @ created on 18.02.2018
 * @see ID
 */
public class ReentrantEntityLocker<E> extends BaseReentrantEntityLocker<E> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReentrantEntityLocker.class);
    private static final Map<Class<?>, Field> CACHED_ENTITY_KEY_FIELDS_BY_CLASS = new WeakHashMap<>();

    /**
     * Данный конструктор предназначен для глобальных блокировок.
     *
     * <p>"Защищенный код", исполняемый в пределах глобальной блокировки,
     * не подвержен конкуренции других потоков вне зависимости от сущностей используемых ими при блокировках,
     * т.е. такой код исполняется в абсолютно эксклюзивном режиме.
     * Таким образом, ни один другой поток не может выполняться в границах "защищенного кода" до тех пор,
     * пока поток, захвативший глобальную блокировку не отпустит её.</p>
     */
    public ReentrantEntityLocker() {
    }

    /**
     * Данный конструктор предназначен для блокировок ключа DO-сущности.
     *
     * <p>"Защищенный код", исполняемый в пределах блокировки значения ключа DO-сущности,
     * является недоступным другим потокам в пределах того же значения ключа DO-сущности.
     * Таким образом, ни один другой поток не может выполниться в границах "защищенного кода"
     * в разрезе блокировки значения ключа DO-сущности до тех пор, пока уже выполняющийся
     * в этих границах в разрезе того же значения ключа DO-сущности поток не выйдет за их пределы.</p>
     */
    public ReentrantEntityLocker(E entity) {
        super(entity);
    }

    @Override
    protected Object getEntityId(E entity) {
        Class<?> entityClass = entity.getClass();

        Object entityId = getEntityIdOrNull(entity, getAnnotatedField(entityClass));
        if (entityId != null) {
            return entityId;
        }

        throw new IllegalArgumentException(
                String.format("Экземпляр класса \'%s\' находится в недопустимом состоянии: %s",
                        entityClass, "являющееся идентификатором операции поле имеет значение null")
        );
    }

    private Field getAnnotatedField(Class<?> entityClass) {
        return CACHED_ENTITY_KEY_FIELDS_BY_CLASS.computeIfAbsent(entityClass, key -> {
            Field annotatedField = getAnnotatedFieldOrNull(entityClass);
            if (annotatedField != null) {
                return annotatedField;
            }
            throw new IllegalArgumentException(
                    String.format("Недопустимо использование экземпляров класса \'%s\' в качестве сущностей синхронизации, " +
                            "т.к. ни одно из полей классов в иерархии наследования не имеет аннотации \'%s\'.", entityClass, ID.class)
            );
        });
    }

    private Field getAnnotatedFieldOrNull(Class<?> entityClass) {
        for (Field field : entityClass.getDeclaredFields()) {
            if (field.isAnnotationPresent(ID.class)) {
                field.setAccessible(true);
                return field;
            }
        }

        Class<?> entitySuperclass = entityClass.getSuperclass();
        return (entitySuperclass != null) && (entitySuperclass != Object.class) ?
                getAnnotatedFieldOrNull(entitySuperclass) : null;
    }

    private Object getEntityIdOrNull(E entity, Field annotatedEntityField) {
        try {
            return annotatedEntityField.get(entity);
        } catch (IllegalAccessException ignore) {
            annotatedEntityField.setAccessible(true);
            return getEntityIdOrNull(entity, annotatedEntityField);
        }
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }
}
