package alm.works.lockers.inherited;

import alm.works.lockers.BaseReentrantEntityLocker;
import alm.works.lockers.EntityLocker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Реализация {@link EntityLocker}, работающая с DO-сущностями, реализующими интерфейс {@link Entity}.
 *
 * @author sbt-yablokov-mv
 * @ created on 18.02.2018
 */
public class ReentrantEntityLocker<T, E extends Entity<T>> extends BaseReentrantEntityLocker<E> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReentrantEntityLocker.class);

    /**
     * Данный конструктор предназначен для глобальных блокировок.
     *
     * <p>"Защищенный код", исполняемый в пределах глобальной блокировки,
     * не подвержен конкуренции других потоков вне зависимости от сущностей используемых ими при блокировках,
     * т.е. такой код исполняется в абсолютно эксклюзивном режиме.
     * Таким образом, ни один другой поток не может выполняться в границах "защищенного кода" до тех пор,
     * пока поток, захвативший глобальную блокировку не отпустит её.</p>
     */
    public ReentrantEntityLocker() {
    }

    /**
     * Данный конструктор предназначен для блокировок ключа DO-сущности.
     *
     * <p>"Защищенный код", исполняемый в пределах блокировки значения ключа DO-сущности,
     * является недоступным другим потокам в пределах того же значения ключа DO-сущности.
     * Таким образом, ни один другой поток не может выполниться в границах "защищенного кода"
     * в разрезе блокировки значения ключа DO-сущности до тех пор, пока уже выполняющийся
     * в этих границах в разрезе того же значения ключа DO-сущности поток не выйдет за их пределы.</p>
     */
    public ReentrantEntityLocker(E entity) {
        super(entity);
    }

    @Override
    protected Object getEntityId(E entity) {
        return entity.getId();
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }
}
