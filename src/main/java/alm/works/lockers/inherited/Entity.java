package alm.works.lockers.inherited;

/**
 * Интерфейс, используемый в качестве маркера для поля, являющегося ключом DO-сущности,
 * на основе которой происходит блокировка доступа к "защищенному коду" в утилитном классе
 * {@link ReentrantEntityLocker}.
 *
 * @author sbt-yablokov-mv
 * @ created on 18.02.2018
 */
public interface Entity<T> {
    T getId();
}
